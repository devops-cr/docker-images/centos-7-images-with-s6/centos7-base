FROM centos:7

RUN yum update -y \
 && yum install -y vim \
 && yum clean all

RUN curl -sSL https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.3/s6-overlay-amd64.tar.gz > /tmp/s6-overlay-amd64.tar.gz \
 && tar xzf /tmp/s6-overlay-amd64.tar.gz -C / --exclude="./bin" \
 && tar xzf /tmp/s6-overlay-amd64.tar.gz -C /usr ./bin


# 0 Continue silently even if any script (fix-attrs or cont-init) has failed.
# 1 Continue but warn with an annoying error message.
# 2 Stop by sending a termination signal to the supervision tree.
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2

ENTRYPOINT ["/init"]
