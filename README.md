# centos7

This is a docker image where we are using the official CentOS 7 dockerhub image and we are installing the S6-Overlay supervisor for our subsequent images. We also set the environment variable `S6_BEHAVIOUR_IF_STAGE2_FAILS` to 2 which means that if a initialization script fails then the container will not be created. 

This image is the base for every image in this subdirectory
